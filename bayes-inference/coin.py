from scipy.stats import bernoulli

class Coin:
    def __init__(self, p):
        self.p = p
        self.q = 1 - p
    def flip(self):
        return bernoulli(self.p).rvs()
    def __str__(self):
        return "P(H) = {:.0%}".format(self.p)
    def __repr__(self):
        return self.__str__()
