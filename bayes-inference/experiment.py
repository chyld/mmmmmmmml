import random
from coin import Coin

def run():
    trials = []
    coins = [Coin(p) for p in [.3, .5, .9]]
    coin = random.choice(coins)
    priors = [1/len(coins) for _ in coins]
    trials.append(priors)
    
    for i in range(10):
        outcome = coin.flip()
        lps = []
        for i, c in enumerate(coins):
            prior = priors[i]
            likelihood = coins[i].p if outcome else coins[i].q
            likelihood_prior = likelihood * prior
            lps.append(likelihood_prior)
        priors = [lp / sum(lps) for lp in lps]
        trials.append(priors)

    return coin, trials
